//Fredrik Carlsson
import java.util.Random;


public class BubbelSort {

	public static void main(String[] args) {
		int size = 10000;
		int max = 1000;
		
		//Create and randomize array
		int [] MyList = new int [size];
		Random generator = new Random();
		for(int i = 0; i < size ; i++){
			MyList[i] = generator.nextInt(max);
		}
		//Print Array Before sort
		for(int i = 0; i < MyList.length ; i++){
			System.out.println(i + ": " + MyList[i]);
		}
		//Send Array to Sort method
		MyList = bubbleSort(MyList);
		
		//Print sorted array
		for(int i = 0; i < MyList.length ; i++){
			System.out.println(i + ": " + MyList[i]);
		}
	}
		//TEhBubbluSOrtueer
		public static int[] bubbleSort(int[] MyList) {
			int temp;
			int j;
			boolean comp = true;
			//While to keep it going.
			while(comp){
				//set comp to false so if(done) break out.
				comp = false;
				//For loop to go thru whole array once. -1 because array starts with 0.
				for(j=0;j<MyList.length -1; j++){
					//If to see if position 1 in array is bigger than position 1+1(position 2)
					if(MyList[j] > MyList[j+1]){
						//save position 1 in temp.
						temp = MyList[j];
						//switch places of pos1 & pos2
						MyList[j] = MyList[j+1];
						MyList[j+1] = temp;
						//set comp to true in order to continue loop.
						comp = true;
					}
				}	
			}
			return MyList;
		}
	

}
